﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
	internal class Soal03
	{
		public Soal03()
		{
			Console.Write("Inputkan huruf dan angka: ");
			char[] input = Console.ReadLine().ToCharArray();


			string pustaka = "abcdefghijklmnopqrstuvwxyz0123456789";

			for (int i = 1; i < input.Length; i++)
			{
				for (int j = i; j > 0; j--)
				{
					if (pustaka.IndexOf(input[j]) < pustaka.IndexOf(input[j - 1]))
					{
						var tamp = input[j];
						input[j] = input[j - 1];
						input[j - 1] = tamp;
					}
				}
			}

			foreach( var item in input)
			{
				Console.Write(item);
			}



		}
	}
}
