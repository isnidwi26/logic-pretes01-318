﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
	internal class Soal01
	{
		public Soal01()
		{
			Console.Write("Topi: ");
			int[] topi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
			Console.Write("Kemeja: ");
			int[] kemeja = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

			int jumlah = 0, maks = 0, min = 0;
			for (int i = 0; i < topi.Length; i++)
			{
				for (int j = 0; j < kemeja.Length; j++)
				{
					jumlah = topi[i] + kemeja[j];
					if (i == 0)
						min = jumlah;
					if (jumlah <= min)
						min = jumlah;
					if (jumlah >= maks)
						maks = jumlah;
				}
			}

			Console.WriteLine($"min:{min} max:{maks}");

		}
	}
}
