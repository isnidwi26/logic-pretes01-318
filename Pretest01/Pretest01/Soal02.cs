﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
	internal class Soal02
	{
        public Soal02()
        {
            Console.Write("Masukkan kata asli: ");
            string kataAsli = Console.ReadLine().ToLower();
            Console.Write("Masukkan kalimat: ");
            string kalimat = Console.ReadLine().ToLower();

            int sum = 0;
            for(int i = 0; i< kalimat.Length; i++)
            {
                if(sum < kataAsli.Length && kalimat[i] == kataAsli[sum])
					sum++;
            }
            Console.WriteLine(sum == kataAsli.Length ? "Yes" : "No");
        }
    }
}
