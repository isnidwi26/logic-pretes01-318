﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal01
	{
        public Soal01()
        {
            Console.WriteLine("=== ROTASI GROUP ===");
            Console.Write("Jumlah Group: ");
            int group = int.Parse(Console.ReadLine());
            Console.Write("Banyak Rotasi: ");
            int rot = int.Parse(Console.ReadLine());
            Console.Write("Data: ");
            string data = Console.ReadLine();

            if (data.Length % group != 0)
                Console.WriteLine("FAIL");
            else
            {
                string rotData = data.Substring(group*rot) + data.Substring(0,(group*rot));
                Console.WriteLine(rotData);
            }
           
        }
    }
}
