﻿namespace Pretest04;

public class Program
{
	public Program()
	{
		Menu();
	}
	static void Main(string[] args)
	{
		Menu();
	}

	static void Menu()
	{
		string answer = "n";
		while (answer.ToLower() == "n")
		{
			Console.WriteLine("\n\t=== Welcome to Pretest 04 ===");
			Console.WriteLine("|   1. ROTASI GROUP			|");
			Console.WriteLine("|   2. DIFFERENCE GANJIL GENAP		|");
			Console.WriteLine("|   3. TRIANGELS NUMBERS		|");
			Console.WriteLine("|   4. SUMMARY ALFABHET AND SORT	|");
			Console.WriteLine("|   5. BELANJA				|");
			Console.WriteLine("|   6. HITUNG BIAYA PARKIR		|");
			Console.WriteLine("|   7. LIBUR BERSAMA			|");
			Console.WriteLine("|   8. ANGKA DARI FIBBONACI DARI URUTAN	|");

			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					Soal01 soal01 = new Soal01();
					break;
				case 2:
					Soal02 soal02 = new Soal02();
					break;
				case 3:
					Soal03 soal03 = new Soal03();
					break;
				case 4:
					Soal04 soal04 = new Soal04();
					break;
				case 5:
					Soal05 soal05 = new Soal05();
					break;
				case 6:
					Soal06 soal06 = new Soal06();
					break;
				case 7:
					Soal07 soal07 = new Soal07();
					break;
				case 8:
					Soal08 soal08 = new Soal08();
					break;
				default:
					break;
			}

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Out from Pretest? [y/n] ");
			answer = Console.ReadLine();
		}
	}
}