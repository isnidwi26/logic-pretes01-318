﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal08
	{
        public Soal08()
        {
            Console.WriteLine("=== ANGKA DARI FIBBONACI DARI URUTAN ===");
            Console.Write("Nilai fibbonaci dari indeks ke: ");
            int indeks = int.Parse(Console.ReadLine());

            int[] fib = new int[indeks];
            for(int i = 0; i < indeks; i++)
            {
                if (i <= 1)
                    fib[i] = 1;
                else
                    fib[i] = fib[i - 2] + fib[i-1];

            }

            Console.WriteLine($"Nilai fibbonaci indeks ke-{indeks}: {fib[indeks - 1]}");
        }
    }
}
