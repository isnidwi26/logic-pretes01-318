﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal02
	{
        public Soal02()
        {
            Console.WriteLine("=== DIFFERENCE GANJIL GENAP ===");
            Console.Write("Input data: ");
            char[] input = Console.ReadLine().ToCharArray();

            int[] data = input.Select(i => Int32.Parse(i.ToString())).ToArray();

            int ganjil = 0, genap = 0;

            for(int i =0; i < data.Length; i++)
            {
                if (data[i] % 2 == 0)
                    genap += data[i];
                else if (data[i] % 2 != 0)
                    ganjil += data[i];
            }

            Console.WriteLine($"ganjil: {ganjil}");
            Console.WriteLine($"genap: {genap}");
            Console.WriteLine($"Hasil: {Math.Abs(genap-ganjil)}");
        }
    }
}
