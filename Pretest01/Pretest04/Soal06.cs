﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal06
	{
        public Soal06()
        {
			Console.WriteLine("\n=====Bayar Parkir=====\n");
			Console.Write("Waktu masuk: ");
			string[] masuk = Console.ReadLine().Split("T");
			string[] dateMasuk = masuk[0].Split("-");
			string[] timeMasuk = masuk[1].Split(":");
			DateTime dtMasuk = new DateTime(int.Parse(dateMasuk[0]), int.Parse(dateMasuk[1]), int.Parse(dateMasuk[2]),
				int.Parse(timeMasuk[0]), int.Parse(timeMasuk[1]), int.Parse(timeMasuk[2]));

			Console.Write("Waktu keluar: ");
			string[] keluar = Console.ReadLine().Split("T");
			string[] dateKeluar = keluar[0].Split("-");
			string[] timeKeluar = keluar[1].Split(":");
			DateTime dtKeluar = new DateTime(int.Parse(dateKeluar[0]), int.Parse(dateKeluar[1]), int.Parse(dateKeluar[2]),
				int.Parse(timeKeluar[0]), int.Parse(timeKeluar[1]), int.Parse(timeKeluar[2]));

			TimeSpan diff = dtKeluar.Subtract(dtMasuk);

			int totalJam = Convert.ToInt32(Math.Round(Convert.ToDecimal(diff.TotalHours), MidpointRounding.ToPositiveInfinity));
			int byrParkir = 0;

			while( totalJam > 0)
			{
				if (totalJam >= 24)
					byrParkir += 50000;
				else
				{
					if (totalJam > 0)
						byrParkir += 5000;
					if(totalJam > 1)
					{
						if (totalJam - 10 > 0)
							byrParkir += 9 * 3000;
						else
							byrParkir += (totalJam - 1) * 3000;
					}
					if(totalJam > 10)
					{
						if (totalJam - 23 > 0)
							byrParkir += 13 * 2000;
						else
							byrParkir += (totalJam - 10) * 2000;
					}
				}
				totalJam -= 24;
			}
			
			Console.WriteLine($"Bayar parkir: {byrParkir}");
		}
    }
}
