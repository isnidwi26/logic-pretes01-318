﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal07
	{
        public Soal07()
        {
            Console.Write("Masukkan tanggal libur Supri: ");
            int supri = int.Parse(Console.ReadLine());

            Console.Write("Masukkan tanggal libur Amir: ");
            int amir = int.Parse(Console.ReadLine());

            Console.Write("Tanggal libur bersamaan: ");
            DateTime libur = DateTime.Parse(Console.ReadLine());

            int tblLibur = (supri + 1) * (amir + 1);

            DateTime tglLbr = libur;

            for (int i = 0; i < tblLibur; i++)
                tglLbr = tglLbr.AddDays(1);

            Console.WriteLine(tglLbr.ToString("dd MMM yyyy"));
        }
    }
}
