﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal05
	{
        public Soal05()
        {
            Console.WriteLine("=== BELANJA 3 ITEM ===");

            Console.Write("Masukkan jumlah uang yang dimiliki: ");
            int uang = int.Parse(Console.ReadLine());

            Console.Write("Deret harga baju: ");
            int[] hrgBaju = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.Write("Deret harga celana: ");
            int[] hrgCelana = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.Write("Deret harga topi: ");
            int[] hrgTopi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int totalHarga=0, maxBelanja = 0;

            for(int i = 0; i < hrgBaju.Length; i++)
            {
                for(int j = 0; j < hrgCelana.Length; j++)
                {
                    for( int k = 0; k < hrgTopi.Length; k++)
                    {
                        totalHarga = hrgBaju[i] + hrgCelana[j] + hrgTopi[k];
                        if(totalHarga <= uang && totalHarga >= maxBelanja)
                            maxBelanja = totalHarga;
                    }
                }
            }

            Console.WriteLine($"Total harga yang sesuai budget adalah: {maxBelanja}");
        }
    }
}
