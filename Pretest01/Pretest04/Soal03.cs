﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
	internal class Soal03
	{
        public Soal03()
        {
            Console.WriteLine("=== Triangle Numbers ===");
            int[,] data = { {0, 9, 4 },{-1, 2, 10 }, {1, 8, 3 } };

            for(int i = 0; i < data.GetLength(0); i++)
            {
                for(int j = 0; j < data.GetLength(1); j++)
                {
                    Console.Write(data[i, j] + "\t");
                }
                Console.WriteLine();
            }

            int segAtas = 0, segBawah = 0;

			for (int i = 0; i < data.GetLength(0); i++)
			{
				for (int j = 0; j < data.GetLength(1); j++)
				{
                    if (i==0 && j>=1)
						segAtas += data[i, j];
					if (j==data.GetLength(0)-1 && i < data.GetLength(0)-1 && i > 0 )
						segAtas += data[i, j];


                    if (j == 0 && i >= 1)
                        segBawah += data[i, j];
					if (i == data.GetLength(0) - 1 && j < data.GetLength(0) - 1 && j > 0)
                        segBawah += data[i, j];

                    if(i==j)
                    {
                        segAtas+= data[i,j];
                        segBawah += data[i,j];
                    }
                }
            }

            Console.WriteLine($"\nsegitiga Atas: {segAtas}");
            Console.WriteLine($"Segitiga Bawah: {segBawah}");
            Console.WriteLine($"Total: {segAtas+ segBawah}");
        }
    }
}
