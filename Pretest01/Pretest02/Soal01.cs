﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
	internal class Soal01
	{
		public Soal01()
		{
			Console.WriteLine("Peminjaman Buku");
			Console.Write("Tanggal pinjam: ");
			string[] pinjam = Console.ReadLine().Split('-');
			Console.Write("Tanggal Kembali: ");
			string[] kembali = Console.ReadLine().Split('-');

			//Kalkulus 5 1000
			//Struktur Data 7 1500
			//Matematika 4 750

			DateTime tglPinjam = new DateTime(int.Parse(pinjam[0]), int.Parse(pinjam[1]), int.Parse(pinjam[2]));
			DateTime tglKembali = new DateTime(int.Parse(kembali[0]), int.Parse(kembali[1]), int.Parse(kembali[2]));

			int total = 0;
			TimeSpan diff = tglKembali.Subtract(tglPinjam);

			Console.WriteLine($"Hari: {diff.TotalDays}");
			if (diff.TotalDays > 4)
			{
				total = (((Convert.ToInt32(diff.TotalDays) - 4) * 750));
				if (diff.TotalDays > 5)
				{
					total += (((Convert.ToInt32(diff.TotalDays) - 5) * 1000));
					if (diff.TotalDays > 7)
					{
						total += (((Convert.ToInt32(diff.TotalDays) - 7) * 1500));
					}
				}
			}
			Console.WriteLine($"Denda: {total}");
		}
	}
}
