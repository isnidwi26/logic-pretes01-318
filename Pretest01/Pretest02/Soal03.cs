﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest02
{
	internal class Soal03
	{
		public Soal03()
		{
			//1 jam pertama 5000 @ jam
			//2 - 7 jam 3000 @ jam
			//8 - 12 jam 2000 @ jam
			//13 - 23 jam 1000 @ jam
			//Kelipatan 24 jam maka lansam 20000
			//Selanjutnya Perhitungan diulangi ke 1 jam pertama

			Console.WriteLine("\n=====Bayar Parkir=====\n");
			Console.Write("Waktu masuk: ");
			string[] masuk = Console.ReadLine().Split("T");
			string[] dateMasuk = masuk[0].Split("-");
			string[] timeMasuk = masuk[1].Split(":");
			DateTime dtMasuk = new DateTime(int.Parse(dateMasuk[0]), int.Parse(dateMasuk[1]), int.Parse(dateMasuk[2]),
				int.Parse(timeMasuk[0]), int.Parse(timeMasuk[1]), int.Parse(timeMasuk[2]));

			Console.Write("Waktu keluar: ");
			string[] keluar = Console.ReadLine().Split("T");
			string[] dateKeluar = keluar[0].Split("-");
			string[] timeKeluar = keluar[1].Split(":");
			DateTime dtKeluar = new DateTime(int.Parse(dateKeluar[0]), int.Parse(dateKeluar[1]), int.Parse(dateKeluar[2]),
				int.Parse(timeKeluar[0]), int.Parse(timeKeluar[1]), int.Parse(timeKeluar[2]));

			TimeSpan diff = dtKeluar.Subtract(dtMasuk);

			int totalJam = Convert.ToInt32(Math.Round(Convert.ToDecimal(diff.TotalHours), MidpointRounding.ToPositiveInfinity));

			int byrParkir = 0;
			if (totalJam <= 1)
			{
				byrParkir = 5000;
			}
			if (totalJam >= 2)
			{
				if (totalJam <= 7)
					byrParkir = 5000 + ((totalJam - 1) * 3000);
				else if (totalJam >= 8 && totalJam <= 12)
					byrParkir = 5000 + ((totalJam - 7) * 2000) + (6 * 3000);
				else if (totalJam >= 13 && totalJam <= 23)
					byrParkir = 5000 + ((totalJam - 12) * 1000) + (6 * 3000) + (5 * 2000);
				else if (totalJam % 24 == 0)
					byrParkir = totalJam / 24 * 20000;
				else if (totalJam > 24)
				{
					if (totalJam % 24 < 2)
						byrParkir = 5000 + (totalJam / 24 * 20000);
					else if (totalJam % 24 <= 7)
						byrParkir = 5000 + (((totalJam % 24) - 1) * 3000) + (totalJam / 24 * 20000);
					else if (totalJam % 24 >= 8 && totalJam % 24 <= 12)
						byrParkir = 5000 + (((totalJam % 24) - 7) * 2000) + (6 * 3000) + (totalJam / 24 * 20000);
					else if (totalJam % 24 >= 13 && totalJam % 24 <= 23)
						byrParkir = 5000 + (((totalJam % 24) - 12) * 1000) + (6 * 3000) + (5 * 2000) + (totalJam / 24 * 20000);
					
				}
					
			}
			Console.WriteLine($"total jam: {totalJam}");
			Console.WriteLine($"Bayar parkir: {byrParkir}");


		}
	}
}
