﻿namespace Pretest02;

public class Program
{
	public Program()
	{
		Menu();
	}
	static void Main(string[] args)
	{
		Menu();
	}

	static void Menu()
	{
		string answer = "n";
		while (answer.ToLower() == "n")
		{
			Console.WriteLine("\n === Welcome to Pretest ===");
			Console.WriteLine("|   1. Denda Buku		|");
			Console.WriteLine("|   2. Bintang		|");
			Console.WriteLine("|   3. Bayar Parkir		|");
			
			Console.Write("Masukkan no soal: ");
			int soal = int.Parse(Console.ReadLine());

			switch (soal)
			{
				case 1:
					Soal01 soal01 = new Soal01();
					break;
				case 2:
					Soal02 soal02 = new Soal02();
					break;
				case 3:
					Soal03 soal03 = new Soal03();
					break;
				default:
					break;
			}

			Console.Write("\nPress any key...");
			Console.ReadKey();
			Console.Write("Out from Pretest? [y/n] ");
			answer = Console.ReadLine();
		}
	}
}