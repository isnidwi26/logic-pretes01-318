﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
	internal class Soal02
	{
        public Soal02()
        {
            string alfabet = "abcdefghijklmnopqrstuvwxyz";

            Console.Write("Inputan: ");
            string huruf = Console.ReadLine().ToLower();
            char[] convHuruf = huruf.ToCharArray();

            int vokal = 0, konsonan = 0;


            for(int i = 0; i < convHuruf.Length; i++)
            {

                if (convHuruf[i] == 'a' || convHuruf[i] == 'i' || convHuruf[i] == 'u' || convHuruf[i] == 'e' || convHuruf[i] == 'o')
                    vokal += alfabet.IndexOf(convHuruf[i])+1;
                else
                    konsonan += alfabet.IndexOf(convHuruf[i])+1;
            }
            Console.WriteLine($"Huruf vokal: {vokal}");
            Console.WriteLine($"Huruf Konsonan: {konsonan}");
            Console.WriteLine($"Selisih vocal & konsonan: {konsonan - vokal}");
        }
    }
}
