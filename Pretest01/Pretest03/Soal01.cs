﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest03
{
	internal class Soal01
	{
		public Soal01()
		{
			Console.Write("Masukkan jumlah uang untuk membeli es krim: ");
			int n = int.Parse(Console.ReadLine());

			int jmlhStik = n / 100, es = 0, sum = 0, sisa = 0;

			if (jmlhStik < 6)
			{
				es = jmlhStik;
			}
			else
			{
				es = jmlhStik;
				sisa = es;

				while (sisa >= 6)
				{
					sum++;
					sisa -= 6;
				}
				sisa = es % 6;
				sisa = sisa + sum;
				if (sisa % 6 == 0)
				{
					while (sisa >= 6)
					{
						sum++;
						sisa -= 6;
					}
				}
			}

			int total = es + sum;

			Console.WriteLine($"Jumlah Es krim yang didapat sebanyak {total}");
		}
	}
}
